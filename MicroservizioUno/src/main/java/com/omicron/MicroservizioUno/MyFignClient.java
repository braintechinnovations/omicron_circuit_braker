package com.omicron.MicroservizioUno;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value="microservizio-2", url="http://localhost:8002/")
public interface MyFignClient {

	@GetMapping
	String responseMsDue();
	
}
