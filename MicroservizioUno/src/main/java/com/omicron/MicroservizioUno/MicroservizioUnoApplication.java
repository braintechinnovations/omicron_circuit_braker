package com.omicron.MicroservizioUno;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@SpringBootApplication
@RestController
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
public class MicroservizioUnoApplication {
	
	@Autowired
	private MyFignClient myFeignClient;
	
	@RequestMapping("/")
	@HystrixCommand(fallbackMethod = "servizioDueFallBack")
	public String messaggioServizioTre() throws JSONException {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("messaggio", "Ciao sono il servizio 1 che risponde!");
		jsonObj.put("messaggio-due", new JSONObject(myFeignClient.responseMsDue()));
		return jsonObj.toString();
	}
	
	public String servizioDueFallBack() throws JSONException {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("messaggio", "Ciao sono il servizio 1 che risponde!");
		jsonObj.put("messaggio-due", "Il servizio due è caduto!");
		return jsonObj.toString();
	}


	public static void main(String[] args) {
		SpringApplication.run(MicroservizioUnoApplication.class, args);
	}

}
