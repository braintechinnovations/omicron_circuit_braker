package com.omicron.MicroservizioDue;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@SpringBootApplication
@RestController
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
@EnableHystrixDashboard
public class MicroservizioDueApplication {

	@Autowired
	private MyFignClient myFeignClient;
	
	@RequestMapping("/")
	@HystrixCommand(fallbackMethod = "servizioTreFallBack")
	public String messaggioServizioTre() throws JSONException {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("messaggio", "Ciao sono il servizio 2 che risponde!");
		jsonObj.put("messaggio-tre", new JSONObject(myFeignClient.responseMsTre()));
		return jsonObj.toString();
	}
	
	public String servizioTreFallBack() throws JSONException {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("messaggio", "Ciao sono il servizio 2 che risponde!");
		jsonObj.put("messaggio-tre", "Il servizio tre è caduto!");
		return jsonObj.toString();
	}

	public static void main(String[] args) {
		SpringApplication.run(MicroservizioDueApplication.class, args);
	}

}
