package com.omicron.MicroservizioDue;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value="microservizio-3", url="http://localhost:8003/")
public interface MyFignClient {

	@GetMapping
	String responseMsTre();
	
}
