package com.omicron.MicroservizioTre;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableEurekaClient
public class MicroservizioTreApplication {
	
	@RequestMapping("/")
	public String messaggioServizioTre() throws JSONException {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("messaggio", "Ciao sono il servizio 3 che risponde!");
		return jsonObj.toString();
	}

	public static void main(String[] args) {
		SpringApplication.run(MicroservizioTreApplication.class, args);
	}

}
